resource "azurerm_subnet" "defaultSubnet" {
  name                 = "default"
  resource_group_name  = var.resource_group_name
  virtual_network_name = azurerm_virtual_network.vnet.name
  address_prefixes     = ["10.0.0.0/24"]
}

resource "azurerm_subnet" "apiManagementSubnet" {
  name                 = "api-management-subnet"
  resource_group_name  = var.resource_group_name
  virtual_network_name = azurerm_virtual_network.vnet.name
  address_prefixes     = ["10.0.1.0/24"]
}

resource "azurerm_subnet_network_security_group_association" "nsgSubnetAssociation" {
  network_security_group_id = azurerm_network_security_group.nsg.id
  subnet_id                 = azurerm_subnet.apiManagementSubnet.id
}

resource "azurerm_virtual_network" "vnet" {
  name                = "${var.resource_group_name}-vnet"
  resource_group_name = var.resource_group_name
  location            = var.resource_location
  address_space       = ["10.0.0.0/16"]
}

resource "azurerm_public_ip" "publicIp" {
  name                = "${var.resource_group_name}-ip-api-management"
  resource_group_name = var.resource_group_name
  location            = var.resource_location
  allocation_method   = "Static"
  sku                 = "Standard"
  domain_name_label   = var.api_management_domain_name_label
}

resource "azurerm_network_security_group" "nsg" {
  name                = "${var.resource_group_name}-nsg"
  resource_group_name = var.resource_group_name
  location            = var.resource_location

  # This rule is only required for external access
  security_rule {
    access                     = "Allow"
    direction                  = "Inbound"
    name                       = "AllowClientCommunicationToApiManagement"
    priority                   = 100
    protocol                   = "Tcp"
    source_port_range          = "*"
    source_address_prefix      = "Internet"
    destination_port_ranges    = [80, 443]
    destination_address_prefix = "VirtualNetwork"
    description                = "Client communication to API Management"
  }

  security_rule {
    access                     = "Allow"
    direction                  = "Inbound"
    name                       = "ApiManagementManagementEndpoint"
    priority                   = 110
    protocol                   = "Tcp"
    source_port_range          = "*"
    source_address_prefix      = "ApiManagement"
    destination_port_range     = 3443
    destination_address_prefix = "VirtualNetwork"
    description                = "Management endpoint for Azure portal and PowerShell"
  }

  security_rule {
    access                     = "Allow"
    direction                  = "Inbound"
    name                       = "LoadBalancerToVirtualNetwork"
    priority                   = 120
    protocol                   = "Tcp"
    source_port_range          = "*"
    source_address_prefix      = "AzureLoadBalancer"
    destination_port_range     = 6390
    destination_address_prefix = "VirtualNetwork"
    description                = "Azure Infrastructure Load Balancer"
  }

  security_rule {
    access                     = "Allow"
    direction                  = "Outbound"
    name                       = "AllowAccessToAzureStorage"
    priority                   = 130
    protocol                   = "Tcp"
    source_port_range          = "*"
    source_address_prefix      = "VirtualNetwork"
    destination_port_range     = 443
    destination_address_prefix = "Storage"
    description                = "Dependency on Azure Storage"
  }

  security_rule {
    access                     = "Allow"
    direction                  = "Outbound"
    name                       = "AllowAccessToAzureSQL"
    priority                   = 140
    protocol                   = "Tcp"
    source_port_range          = "*"
    source_address_prefix      = "VirtualNetwork"
    destination_port_range     = 1443
    destination_address_prefix = "Sql"
    description                = "Access to Azure SQL endpoints"
  }

  security_rule {
    access                     = "Allow"
    direction                  = "Outbound"
    name                       = "AllowAccessToAzureKeyVault"
    priority                   = 150
    protocol                   = "Tcp"
    source_port_range          = "*"
    source_address_prefix      = "VirtualNetwork"
    destination_port_range     = 443
    destination_address_prefix = "AzureKeyVault"
    description                = "Access to Azure Key Vault"
  }
}

resource "azurerm_api_management" "apiManagement" {
  name                = "${var.resource_group_name}-api-mgmt"
  location            = var.resource_location
  resource_group_name = var.resource_group_name
  publisher_name      = var.api_management_publisher_name
  publisher_email     = var.api_management_publisher_email
  sku_name            = var.api_management_sku_name

  public_ip_address_id = azurerm_public_ip.publicIp.id
  virtual_network_type = "External"
  virtual_network_configuration {
    subnet_id = azurerm_subnet.apiManagementSubnet.id
  }
}