variable "resource_group_name" {
  type = string
}

variable "resource_location" {
  type = string
  default = "East Asia"
}

variable "api_management_publisher_name" {
  type = string
}

variable "api_management_publisher_email" {
  type = string
}

variable "api_management_domain_name_label" {
  type = string
}