resource "azurerm_api_management" "apiManagement" {
  name                = "${var.resource_group_name}-api-mgmt"
  location            = var.resource_location
  resource_group_name = var.resource_group_name
  publisher_name      = var.api_management_publisher_name
  publisher_email     = var.api_management_publisher_email
  sku_name            = "Consumption_0"
}